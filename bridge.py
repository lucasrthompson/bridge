import sys, random
import pygame
from pygame.locals import *
import pymunk
import pymunk.pygame_util
import neat
import numpy as np
import neat
import pickle


DX = 600
DY = 600


def eval_genomes(genomes, config):
        
    fitness = 0

    for genome_id, genome in genomes:
        print(genome_id,fitness)
        
        pygame.init()
        screen = pygame.display.set_mode((DX, DY))
        pygame.display.set_caption("Joints. Just wait and the L will tip over")
        clock = pygame.time.Clock()

        space = pymunk.Space()
        space.gravity = (0.0, -900.0)

        
        balls = []
        draw_options = pymunk.pygame_util.DrawOptions(screen)

        ticks_to_next_ball = 10
        net = neat.nn.recurrent.RecurrentNetwork.create(genome, config)
        
        
        inputs = net.activate([fitness])
        add_bridge(space,inputs)
        
        fitness = 0
        done = False
        frame = 0
        
        
        while not done:
            frame += 1
            
            ticks_to_next_ball -= 1
            if ticks_to_next_ball <= 0:
                ticks_to_next_ball = 5
                ball_shape = add_ball(space)
                balls.append(ball_shape)

            balls_to_remove = []
            for ball in balls:
                if ball.body.position.y < 150:
                    #print("Done!")
                    balls_to_remove.append(ball)
                    done = True
                    fitness = frame
                    
                    break
    
            
            for ball in balls_to_remove:
                space.remove(ball, ball.body)
                balls.remove(ball)
                
            

     


            
            
            space.step(1/50.0)

            screen.fill((255,255,255))
            space.debug_draw(draw_options)

            pygame.display.flip()
            clock.tick(50)

        genome.fitness = fitness
                
            
            


def add_ball(space):
    """Add a ball to the given space at a random position"""
    mass = 1
    radius = 14
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
    body = pymunk.Body(mass, inertia)
    x = random.randint(120,380)
    body.position = x, 550
    shape = pymunk.Circle(body, radius, (0,0))
    space.add(body, shape)
    return shape

def add_L(space):
    """Add a inverted L shape with two joints"""
    rotation_center_body = pymunk.Body(body_type = pymunk.Body.STATIC)
    rotation_center_body.position = (300,300)

    rotation_limit_body = pymunk.Body(body_type = pymunk.Body.STATIC)
    rotation_limit_body.position = (200,300)

    body = pymunk.Body(10, 10000)
    body.position = (300,300)
    l1 = pymunk.Segment(body, (-150, 0), (255.0, 0.0), 1)
    l2 = pymunk.Segment(body, (-150.0, 0), (-150.0, 50.0), 1)

    rotation_center_joint = pymunk.PinJoint(body, rotation_center_body, (0,0), (0,0))
    joint_limit = 25
    rotation_limit_joint = pymunk.SlideJoint(body, rotation_limit_body, (-100,0), (0,0), 0, joint_limit)

    space.add(l1, l2, body, rotation_center_joint, rotation_limit_joint)
    return l1,l2

def add_bridge(space, inputs):
    
    b1 = pymunk.Body()
    b1.position = (100,100)
    b2 = pymunk.Body()
    b2.position = (150,100)
    b3 = pymunk.Body()
    b3.position = (200,100)
    b4 = pymunk.Body()
    b4.position = (125,150)
    b5 = pymunk.Body()
    b5.position = (175,150)
    
    
    l1 = pymunk.Segment(b1, (b1.position[0] - 25, b1.position[1]), (b1.position[0] + 25, b1.position[1]), 5)
    l2 = pymunk.Segment(b2, b2.position, b3.position, 5)
    l3 = pymunk.Segment(b3, b3.position, b4.position, 5)
    l4 = pymunk.Segment(b4, b4.position, b5.position, 5)
    l5 = pymunk.Segment(b5, b5.position, b2.position, 5)
    
    #rotation_center_joint = pymunk.PinJoint(body, rotation_center_body, edges[i-1][0], edges[i][0])
    space.add(l1,l2,l3,l4,l5)  
    
    #points = []
    #edges = []
    #for p in range(0,19):
        #points.append([inputs[p]*DX,inputs[p+10]*DY])
        
    #for p in range(1, len(points)):
        #edges.append([points[p-1], points[p]])
        
    
    #for i in range(1, len(edges)):
        
        
        #b1 = pymunk.Body(body_type = pymunk.Body.STATIC)
        #b1.position = (edges[i-1][0])
        #b2 = pymunk.Body(body_type = pymunk.Body.STATIC)
        #b2.position = (edges[i][0])
        #body = pymunk.Body(10, 10000)
        #body.position = (edges[i-1][0])
        #l1 = pymunk.Segment(body, edges[i-1][0], edges[i][0], 1)
        #rotation_center_joint = pymunk.PinJoint(body, rotation_center_body, edges[i-1][0], edges[i][0])
        #space.add(l1, body, rotation_center_joint)
        

def main():
    
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        'config-feedforward')

    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(10))

    #p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-10')

    winner = p.run(eval_genomes)

    #with open('winner.pkl', 'wb') as output:
        #pickle.dump(winner, output, 1)

    

if __name__ == '__main__':
    main()
